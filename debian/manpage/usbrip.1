.TH usbrip 1 "September 2023" "USB device artifacts tracker"

.SH NAME
\fBusbrip\fP - USB device artifacts tracker
.SH SYNOPSIS
\fBusbrip\fP [\fBOPTIONS\fP] [\fBDOMAINS [DOMAINS ...]\fP]
.SH DESCRIPTION
.PP
.nf
.fam C
USBRip is a command-line tool for tracking USB device artifacts (USB event history)
on GNU/Linux. It allows you to investigate and analyze USB device-related events
on a Linux system.
.fam T
.fi
.SH USAGE
To use USBRip, you can use the following subcommands and options:
.PP
.nf
.fam C
.B Events
.PP
.B ~$ usbrip events history
   [-t | -l] [-e] [-n <NUMBER_OF_EVENTS>]
   [-d <DATE> [<DATE> ...]] [--host <HOST> [<HOST> ...]] [--vid
   <VID> [<VID> ...]] [--pid <PID> [<PID> ...]] [--prod <PROD>
   [<PROD> ...]] [--manufact <MANUFACT> [<MANUFACT> ...]] [--serial
   <SERIAL> [<SERIAL> ...]] [--port <PORT> [<PORT> ...]] [-c <COLUMN>
   [<COLUMN> ...]] [-f <FILE> [<FILE> ...]] [-q] [--debug]
   Get USB event history.
.PP
.B ~$ usbrip events open
   <DUMP.JSON> [-t | -l] [-e] [-n <NUMBER_OF_EVENTS>]
   [-d <DATE> [<DATE> ...]] [--host <HOST> [<HOST> ...]]
   [--vid <VID> [<VID> ...]] [--pid <PID> [<PID> ...]]
   [--prod <PROD> [<PROD> ...]] [--manufact <MANUFACT> [<MANUFACT> ...]]
   [--serial <SERIAL> [<SERIAL> ...]] [--port <PORT> [<PORT> ...]]
   [-c <COLUMN> [<COLUMN> ...]] [-q] [--debug]
.PP
.B ~$ sudo usbrip events genauth
   <OUT_AUTH.JSON> [-a <ATTRIBUTE> [<ATTRIBUTE> ...]]
   [-e] [-n <NUMBER_OF_EVENTS>] [-d <DATE> [<DATE> ...]]
   [--host <HOST> [<HOST> ...]] [--vid <VID> [<VID> ...]]
   [--pid <PID> [<PID> ...]] [--prod <PROD> [<PROD> ...]]
   [--manufact <MANUFACT> [<MANUFACT> ...]]
   [--serial <SERIAL> [<SERIAL> ...]] [--port <PORT> [<PORT> ...]]
   [-f <FILE> [<FILE> ...]] [-q] [--debug]
.PP
.B ~$ sudo usbrip events violations
   <IN_AUTH.JSON> [-a <ATTRIBUTE> [<ATTRIBUTE> ...]]
   [-t | -l] [-e] [-n <NUMBER_OF_EVENTS>] [-d <DATE> [<DATE> ...]]
   [--host <HOST> [<HOST> ...]] [--vid <VID> [<VID> ...]]
   [--pid <PID> [<PID> ...]] [--prod <PROD> [<PROD> ...]]
   [--manufact <MANUFACT> [<MANUFACT> ...]]
   [--serial <SERIAL> [<SERIAL> ...]] [--port <PORT> [<PORT> ...]]
   [-c <COLUMN> [<COLUMN> ...]] [-f <FILE> [<FILE> ...]] [-q] [--debug
.PP
.B Storage
.B Busbrip storage list <STORAGE_TYPE> [-q] [--debug]
   List contents of the selected storage. \fBSTORAGE_TYPE\fP can be "history" or
   "violations".
.PP
.B IDs
.B ~$ usbrip ids search
   [--vid <VID>] [--pid <PID>] [--offline] [-q] [--debug]
   Get extra details about a specific USB device by its <VID> and/or <PID> from
   the USB ID database.
.PP
.B ~$ usbrip ids download [-q] [--debug]
   Update (download) the USB ID database.
.fam T
.fi
.SH COMMON OPTIONS
.PP
.nf
.fam C
Some common options include:
\fB-h\fP, \fB--help\fP: Show this help message.
\fB-t\fP [target URL]: Specify the target URL.
\fB-w\fP [word list]: Specify the word list to be tested.
\fB-T\fP [tasks]: Set the number of connections in parallel (per host).
\fB-o\fP [output file]: Save the output to disk.
\fB-x\fP [extensions]: Append extensions to each request (comma-separated values).
.fam T
.fi
.SH EXAMPLES
.B Usage examples:
.PP
.nf
.fam C
Show the event history of all USB devices, suppressing banner output, info
messages, and user interaction (-q, --quiet), represented as a list (-l, --list)
with the latest 100 entries (-n NUMBER, --number NUMBER):
\fBExample: ~$ usbrip events history -ql -n 100\fP
.PP
Show the event history of external USB devices (-e, --external, which were
actually disconnected), represented as a table (-t, --table) containing
Connected, VID, PID, Disconnected, and Serial Number columns (-c COLUMN
[COLUMN ...], --column COLUMN [COLUMN ...]) filtered by date (-d DATE [DATE ...],
--date DATE [DATE ...]) and PID (--pid <PID> [<PID> ...]) with logs taken from
external files (-f FILE [FILE ...], --file FILE [FILE ...]):
\fBExample: ~$ usbrip events history -et -c conn vid pid disconn serial -d '1995-09-15' '2018-07-01' --pid 1337 -f /var/log/syslog.1 /var/log/syslog.2.gz\fP
.PP
\fBNote\fP: When working with filters, remember that there are four types of
filtering available: only external USB events (devices that can be easily
\fB disconnected, -e), by date (-d), by fields (--host, --vid, --pid, --product,
--manufact, --serial, --port)\fP., and by the number of entries you get as the
output (-n). When applying different filters simultaneously, the following
behavior occurs: firstly, external and by date filters are applied, then
USBRip will search for specified field values in the intersection of the
last two filters, and finally, it will cut the output to the number you
defined with the -n option. So, think of it as an intersection for external
and by date filtering and union for by fields filtering. Hope it makes sense.
.PP
\fBBuild the event history of all USB devices and redirect the output to a file\fP
for further analysis. When the output stream is NOT terminal stdout (| or >,
for example), there will be no ANSI escape characters (color) in the output,
so feel free to use it that way. Also, note that USBRip uses some UNICODE
symbols, so it would be nice to convert the resulting file to UTF-8 encoding
(with encov, for example) as well as change newline characters to Windows style
for portability (with awk, for example):
\fB Example: $ usbrip events history -t | awk '{ sub("$", "\n"); print }' > usbrip.out && enconv -x UTF8 usbrip.out\fP
.PP
\fBNote\fP: You can always remove the escape characters by yourself even if you
have already got the output to stdout. To do that, just copy the output data
to usbrip.out and apply one more awk instruction:
\fBExample: ~$ awk '{ sub("$", "\n"); gsub("\\x1B\\[[0-?]*[ -/]*[@-~]", ""); print }' usbrip.out && enconv -x UTF8 usbrip.out\fP
.PP
\fBGenerate a list of trusted USB devices as a JSON file (trusted/auth.json)\fP
with VID and PID attributes containing the first three devices connected
on November 30, 1984:
\fBExample: ~$ sudo usbrip events genauth trusted/auth.json -a vid pid -n 3 -d '1984-11-30'\fP
.PP
\fBWarning:\fP There are cases when different USB flash drives might have identical
serial numbers. This could happen as a result of a manufacturing error or just
some black hats were able to rewrite the drive's memory chip which turned out to
be non-one-time programmable and so on... Anyways, "no system is safe". USBRip
does not handle such cases in a smart way so far, namely it will treat a pair
of devices with identical SNs (if there exists one) as the same device regarding
the trusted device list and genauth module.
.PP
\fBSearch the event history of the external USB devices for violations based on
the list of trusted USB devices\fP (trusted/auth.json) by PID attribute, restrict
resulting events to those which have Bob-PC as a hostname, EvilUSBManufacturer
as a manufacturer, 0123456789 as a serial number, and represent the output as a
table with Connected, VID, and PID columns:
\fB~$ sudo usbrip events violations trusted/auth.json -a pid -et --host Bob-PC --manufact EvilUSBManufacturer --serial 0123456789 -c conn vid pid\fP
.PP
\fBSearch for details about a specific USB device by its VID (--vid VID) and PID (--pid PID):\fP
\fBExample: ~$ usbrip ids search --vid 0781 --pid 5580\fP
.PP
.B Where: DOMAINS represents one or more domains to perform USB forensics on.
\fBFor example:\fP
To analyze a single domain, use:
\fBusbrip example.com\fP
.PP
To analyze multiple domains, use:
\fBusbrip example.com example.org example.net\fP
.PP
.B You can also specify various OPTIONS to customize the USB forensics process.
\fBFor example:\fP
To show verbose output, use:
\fBusbrip ‐v example.com\fP
.fam T
.fi
.SH AUTHOR
.fam C
.nf
Developed by Snovvcrash <snovvcrash@protonmail.ch>
.PP
This manual page was written by Josenison Ferreira da Silva <nilsonfsilva@hotmail.com>
for the Debian project (but may be used by others).
.fam T
.fi

#!/usr/bin/python3

import sys
import os

sys.path.insert(0, '/usr/share/usbrip')

from usbrip.__main__ import main

if __name__ == '__main__':
    main()
